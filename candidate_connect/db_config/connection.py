import pymongo
from pymongo import MongoClient

DATABASE_NAME = 'candidate_connect_db'
DATABASE_USER = 'candidate_connect'
DATABASE_PASSWORD = 'candidate_connect'
DATABASE_HOST = 'localhost'
DATABASE_PORT = 27017

#connect to mongodb client instance
client = MongoClient(
	'{0:1}'.format(DATABASE_HOST,DATABASE_PORT),
	username = DATABASE_USER,
	password = DATABASE_PASSWORD,
	authSource=DATABASE_NAME
	)

#set db name to access
db = client['candidate_connect_db']
