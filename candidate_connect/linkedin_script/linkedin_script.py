
from selenium import webdriver
import time
from bs4 import BeautifulSoup
import json
import os

class Linked_In_Page:
    BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    driver = webdriver.Firefox(executable_path=os.path.join(BASE_DIR, 'geckodriver/geckodriver'))
    urls = []
    final_url = []

    responce = {
        'Login successful' : '',
        'Login failed': '',
        'error': ''
    }

    def Login(self, id, password):

        self.id = id
        self.password = password
        self.driver.get("https://www.linkedin.com/")

        self.driver.find_element_by_xpath("//a[@class = 'nav__button-secondary']").click()
        self.driver.find_element_by_xpath("//input[@id = 'username'][@name = 'session_key']").send_keys(self.id)
        self.driver.find_element_by_xpath("//input[@id = 'password'][@type = 'password']").send_keys(self.password)
        self.driver.find_element_by_xpath(
                "//button[@class = 'btn__primary--large from__button--floating'][@type = 'submit']").click()

        time.sleep(10)

        if self.driver.title == "LinkedIn Login, LinkedIn Sign in | LinkedIn":
            src = self.driver.page_source
            soup = BeautifulSoup(src, 'html.parser')
            if self.driver.find_element_by_xpath("//div[@id= 'error-for-password']"):
                pswd_err= soup.find('div', id="error-for-password").get_text()
                time.sleep(10)
                self.responce["Login successful"]= False
                self.responce["Login failed"]= True
                self.responce["error"]= pswd_err
                return self.responce
            else:
                if self.driver.find_element_by_xpath("//div[@id='error-for-username']"):
                    id_err = soup.find('div', id="error-for-username").get_text()
                    self.responce["Login Successful"]= False
                    self.responce["Login failed"]= True
                    self.responce["error"]= id_err
                    return self.responce

        elif (self.driver.title == "LinkedIn Login, Sign in | LinkedIn"):
            src = self.driver.page_source
            soup = BeautifulSoup(src, 'html.parser')
            if self.driver.find_element_by_xpath("//div[@id= 'error-for-password']"):
                pswd_err= soup.find('div', id="error-for-password").get_text()
                time.sleep(10)
                self.responce["Login successful"]= False
                self.responce["Login failed"]= True
                self.responce["error"]= pswd_err
                return self.responce
            else:
                if self.driver.find_element_by_xpath("//div[@id='error-for-username']"):
                    id_err = soup.find('div', id="error-for-username").get_text()
                    self.responce["Login Successful"]= False
                    self.responce["Login failed"]= True
                    self.responce["error"]= id_err
                    return self.responce
        else:
            self.responce["Login successful"]= True
            self.responce["Login failed"]= False
            self.responce["error"]= False
            return self.responce

    def search_candidate(self, key_word, num_prfl):
        responce = {
        'Search Candidates Successful': '',
        'Search Candidates Failed': '',
        'error': '',
        'data': '',
        }

        try:
            self.key_word = key_word
            self.driver.get("https://www.linkedin.com/feed")

            self.driver.find_element_by_xpath(
                    "//input[@class = 'search-global-typeahead__input always-show-placeholder'][@type = 'text']").send_keys(
                    key_word)
            self.driver.find_element_by_xpath("//div[@class = 'search-global-typeahead__controls']").click()
            time.sleep(5)

            self.driver.find_element_by_xpath("//button[@class ='search-vertical-filter__filter-item-button artdeco-button artdeco-button--muted artdeco-button--2 artdeco-button--tertiary ember-view'][@type='button']").click()
            time.sleep(5)

            candidates = []
            self.num_prfl = num_prfl/5

            for num in range(0, int(self.num_prfl)):

                src = self.driver.page_source
                soup = BeautifulSoup(src, 'html.parser')
                time.sleep(10)

                list = []

                page = soup.find('div', class_="search-results ember-view")
                li = page.find_all('li', class_="search-result search-result__occluded-item ember-view")
                # print(len(li))
                list.extend(li)

                for i in range(0, len(list)):

                    dict = {}

                    li1 = list[i]
                    time.sleep(2)
                    try:
                        link = li1.find('a', class_="search-result__result-link ember-view")['href']
                    except Exception as e:
                        dict['url'] = 'not available'
                    else:
                        dict['url'] = "www.linkedin.com" + str(link)
                    # print('url')
                    # scraping_name
                    try:
                        name_loc = li1.find('div', class_="search-result__info pt3 pb4 ph0")
                    except Exception as e:
                        dict['name'] = 'not available'
                    else:
                        try:
                            name_scrap = name_loc.find('a', class_="search-result__result-link ember-view")
                        except Exception as e:
                            dict['name'] = 'not available'
                        else:
                            try:
                                name_scrap_2 = name_scrap.find('span', class_="name actor-name").get_text()
                            except Exception as e:
                                dict['name'] = name_scrap_2
                            else:
                                dict['name'] = name_scrap_2
                    # print('name')
                    #scraping image url
                    try:
                        image = li1.find('figure', class_="search-result__image")
                    except Exception as e:
                        dict['img_url'] = 'not available'
                    else:
                        try:
                            img_src = image.find('img')
                        except Exception as e:
                            dict['img_url'] = 'not available'
                        else:
                            try:
                                image= img_src['src']
                            except Exception as e:
                                dict['img_url']= e
                            else:
                                dict['img_url']= image
                    # print("image url")


                    #scraping designation
                    try:
                        job_title = li1.find('div', class_="search-result__info pt3 pb4 ph0")
                    except Exception as e:
                        dict['designation']= 'not available'
                    else:
                        try:
                            job_title2 = job_title.find('p',
                                                        class_="subline-level-1 t-14 t-black t-normal search-result__truncate").get_text()
                        except Exception as e:
                            dict['designation']= 'not available'
                        else:
                            str1= str(job_title2)
                            str1= str1.split('\n')
                            str1= str1[1]
                            dict['designation']= str1
                    # print("designation")


                    #scraping location
                    try:
                        location = li1.find('div', class_="search-result__info pt3 pb4 ph0")

                    except Exception as e:
                        dict['location'] = 'not available'
                    else:
                        try:
                            loc = location.find('p',
                                                class_='subline-level-2 t-12 t-black--light t-normal search-result__truncate')
                        except Exception as e:
                            dict['location'] = 'not available'
                        else:
                            try:
                                loc2 = loc.find('span').get_text()
                            except Exception as e:
                                dict['location'] = 'not available'
                            else:
                                dict['location'] = loc2
                    # print("location")


                    # scraping_summary
                    try:
                        smry = li1.find('div', class_="search-result__info pt3 pb4 ph0")
                    except Exception as e:
                        # smry_loc = e
                        dict['summary']= 'not available'
                    else:
                        try:
                            smry_loc= smry.find('p',
                                                 class_="mt2 t-12 t-black--light t-normal search-result__snippets-black").get_text().strip()
                        except Exception as e:
                            dict['summary']= 'not available'
                        else:
                            dict['summary']= smry_loc
                    candidates.append(dict)

                # scrolling page till end
                self.driver.execute_script("window.scrollBy(0, document.body.scrollHeight)")
                time.sleep(10)
                # getting next_page link and clicking it
                self.driver.find_element_by_xpath(
                    "//button[@class='artdeco-pagination__button artdeco-pagination__button--next artdeco-button artdeco-button--muted artdeco-button--icon-right artdeco-button--1 artdeco-button--tertiary ember-view']").click()
                time.sleep(10)
                # getting current url and giving it to browser.get()
                current_link = self.driver.current_url
                self.driver.get(current_link)

            responce['Search Candidates Successful']= True
            responce['Search Candidates Failed']= False
            responce['error']= False
            responce['data']= candidates
            return responce

        except Exception as e:
            responce['Search Candidates Successful']= False
            responce['Search Candidates Failed']= True
            responce['error']= e
            responce['data']= []
            return responce

    def scrap_candidate(self, url):
        responce = {
        'Scrap Candidate Successful' : '',
        'Scrap Candidate Failed': '',
        'error': '',
        'data': '',
        }

        try:
            self.driver.get(url)
            time.sleep(5)
            scrp_candidate= []
            dict1= {'experience': [] , 'education': [], 'skills':[]}

            src = self.driver.page_source
            soup = BeautifulSoup(src, 'html.parser')

            div= soup.find('div', class_='display-flex mt2')

            name = div.find('li', class_='inline t-24 t-black t-normal break-words').get_text().strip()
            dict1['name: '] = name

            designation= div.find('h2', class_='mt1 t-18 t-black t-normal').get_text().strip()
            dict1['designation']= designation

            location = div.find('li', class_='t-16 t-black t-normal inline-block').get_text().strip()
            dict1['location']= location

            #scraping the exp
            exp_div = soup.find('div', class_='pv-profile-section-pager ember-view')
            exp_li = exp_div.find_all('li', class_='pv-entity__position-group-pager pv-profile-section__list-item ember-view')

            for i in range(0, len(exp_li)):
                dict2= {}
                profile_name = exp_li[i].find('h3', class_='t-16 t-black t-bold').get_text().strip()
                dict2['profile_name']= profile_name

                #getting duration of work
                cmpny_name = exp_li[i].find('p', class_='pv-entity__secondary-title t-14 t-black t-normal').get_text().strip()
                dict2['company_name']= cmpny_name

                duration = exp_li[i].find('h4', class_='pv-entity__date-range t-14 t-black--light t-normal').get_text().strip()
                #here manipulate
                string= str(duration)
                string= string.split("\n")
                duration2= string[-1]
                dict2['duration']= duration2
                dict1['experience'].append(dict2)

            #scraping the edu:
            edu_div= soup.find_all('div', class_='pv-profile-section-pager ember-view')
            edu_li= edu_div[1].find_all('li', class_='pv-profile-section__list-item pv-education-entity pv-profile-section__card-item ember-view')
            for i in range(0, len(edu_li)):
                dict3= {}
                clg_name= edu_li[i].find('h3', class_='pv-entity__school-name t-16 t-black t-bold').get_text().strip()
                dict3['clg_name']= clg_name
                degree = edu_li[i].find_all('p', class_='pv-entity__secondary-title pv-entity__degree-name t-14 t-black t-normal')
                for i in range(0, len(degree)):
                    degree_info= degree[i].find('span').get_text().strip()
                    dict3['degree_info']= degree_info
                degree_yr= edu_li[i].find('p', class_='pv-entity__dates t-14 t-black--light t-normal').get_text().strip()

                string= str(degree_yr)
                string= string.split("\n\n")
                degree_yr2= string[-1]
                dict3['degree_year']= degree_yr2
                dict1['education'].append(dict3)

            self.driver.execute_script("window.scrollBy(0, document.body.scrollHeight)")
            time.sleep(10)

            self.driver.find_element_by_xpath("//button[@class='pv-profile-section__card-action-bar pv-skills-section__additional-skills artdeco-container-card-action-bar artdeco-button artdeco-button--tertiary artdeco-button--3 artdeco-button--fluid']").click()
            time.sleep(10)
            src = self.driver.page_source
            soup = BeautifulSoup(src, 'html.parser')
            skl_lst= []
            span= soup.find_all('span', class_='pv-skill-category-entity__name-text t-16 t-black t-bold')
            time.sleep(10)
            for i in range(0, len(span)):
                spn= span[i].get_text().strip()
                skl_lst.append(spn)

            dict1['skills']=skl_lst

            scrp_candidate.append(dict1)

            responce['Scrap Candidate Successful']= True
            responce['Scrap Candidate Failed']= False
            responce['error']= False
            responce['data']= scrp_candidate
            self.driver.close()
            return responce
        except Exception as e:
            responce['Scrap Candidate Successful']= False
            responce['Scrap Candidate Failed']= True
            responce['error']= e
            responce['data']= []
            self.driver.close()
            return responce
