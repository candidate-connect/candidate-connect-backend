from datetime import datetime
import json

from bson.objectid import ObjectId
from django.http import JsonResponse
from django.contrib.auth.hashers import check_password, make_password
import jwt
from rest_framework import status
from rest_framework.decorators import api_view, authentication_classes, permission_classes
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
#from rest_framework_jwt.authentication import JSONWebTokenAuthentication, CustomJSONWebTokenAuthentication
from rest_framework_jwt.settings import api_settings

from candidate_connect.settings import APP_ACCESS_ROLES
from .authentication import CustomJSONWebTokenAuthentication
from .forms import SignUpForm, LoginForm, UpdateProfileForm, ChangePasswordForm, ForgotPasswordForm, LinkedInLoginForm
from .forms import ResetPasswordForm
from .models import Model
from .permissions import IsAuthenticated

# from .practice import test
jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER
jwt_decode_handler = api_settings.JWT_DECODE_HANDLER

model = Model()
# from linkedin_script.linkedin_script import *
# linkedin_api = Linked_In_Page()

@api_view(['POST'])
@permission_classes((AllowAny,))
def signup(request):
	"""
		Used to insert users signup data into system
	"""
	if request.method == 'POST':
		#get the request signup json data
		form_data = SignUpForm(request.POST)
		response = {
			'status':'',
			'message':None,
			'errors':{}		
		}
		if form_data.is_valid():
			form_data = form_data.cleaned_data
			#hashed the plain password using django password hasher
			encrpt_password = make_password(form_data['password'])
			#get the first and last name from input json
			first_name = form_data['first_name']
			last_name = form_data['last_name']
			#get the user role_details for the respective role
			role_info = model.get_user_role_details(form_data.get('role'))
			#create a json to insert into users table
			data_response = {
				'first_name':first_name,
				'last_name':last_name,
				'user_name':'{0} {1}'.format(first_name,last_name), #combine first , lastname to give username
				'gender':form_data['gender'],
				'email':form_data['email'],
				'password':encrpt_password,
				'role':role_info, #set the role id which is got from roles table
				'self_authenticate':True, #key to check the user has activated their account
				'portal_authenticate':False, #key to check the user account has got approval or not
				'created_date':datetime.now(),
				'modified_date':datetime.now(),
				'is_active':True,
				'is_deleted':False,
			}
			inserted_record = model.insert_signup_data(data_response)
			if inserted_record:
				response['status'] = status.HTTP_200_OK
				response['message'] = 'User has created successfully.'
			else:
				response['status'] = status.HTTP_400_BAD_REQUEST
				response['message'] = 'Try Your signup again.'
		else:
			response['status'] = status.HTTP_400_BAD_REQUEST
			response['message'] = 'Check your form fields.'
			response['errors'] = dict(form_data.errors.items())
		return Response(response, status=response['status'])

@api_view(['POST'])
def is_signup_email_exists(request):
	"""
	Used to check wether the email 
	already registered in portal before during signup
	"""
	if request.method == 'POST':
		email = request.data.get('email')
		count = model.is_email_present(email)
		if count:
			message = 'User already exists in record.'
		else:
			message = 'User not exists.'
		response = {
			'status':'ok',
			'message':message,
			'count':count,
		}
		return JsonResponse(response)

@api_view(['POST'])
@permission_classes((AllowAny,))
def login(request):

	"""
	Used to validate and Authenticate the user
	signin credentials
	"""
	if request.method == 'POST':
		form_data = LoginForm(request.POST)
		#json response format
		response = {
			'status':'',
			'message':None,
			'errors':{},
		}
		#validate the form with login form attributes
		if form_data.is_valid():
			form_data = form_data.cleaned_data
			user_email = form_data['email']
			user_password = form_data['password']
			#get the hashed password of the user for the given email
			#if its exists in records.
			login_details = model.get_user_hashed_password(user_email)
			#if user exist compare the raw password with hashed password
			#using django authentication
			if login_details:
				password_isvalid = check_password(user_password,login_details['password'])
				#check the password are matched
				if password_isvalid:
					if login_details['is_active'] is False:
						response['status'] = status.HTTP_401_UNAUTHORIZED
						response['message'] = 'User account is in inactive state.'
					elif login_details['self_authenticate'] is False:
						response['status'] = status.HTTP_401_UNAUTHORIZED
						response['message'] = 'User account email verification failed.'
					else:
						payload = model.get_user_payload_details(login_details['email'])
						payload = jwt_payload_handler(payload)
						data = {'token': jwt_encode_handler(payload),'user': payload}
						response['message'] = 'Token will valid for 1day.'
						response['data'] = data
						response['status'] = status.HTTP_200_OK
				else:
					response['status'] = status.HTTP_401_UNAUTHORIZED
					response['message'] = 'Password is invalid.'
			else:
				response['status'] = status.HTTP_401_UNAUTHORIZED
				response['errors']['email'] = 'Email/Password incorrect'
				response['message'] = 'Email/Password incorrect'
		else:
			response['status'] = status.HTTP_400_BAD_REQUEST
			response['message'] = 'Check your form fields.'
			response['errors'] = dict(form_data.errors.items())
		return Response(response,status=response['status'])

@api_view(['POST'])
def generate_password(request):
	pass
	password = request.data.get('password')
	return JsonResponse({'status':make_password(password)})

@api_view(['POST'])
def validate_password(request):
	password = request.data.get('password')
	hashed = 'pbkdf2_sha256$180000$S2SLZXFo8MBA$U8RCkIpsiPnxr8G3nhoDg7xomSB+F4bfgc17htO/t4s='
	return JsonResponse({'status':check_password(password,hashed)})


@api_view(['GET'])
@permission_classes((IsAuthenticated,))
@authentication_classes((CustomJSONWebTokenAuthentication,))
def posts(request):
	#response = test()
	data = {'key':'issere'}
	return JsonResponse(data)

@api_view(['POST'])
@permission_classes((AllowAny,))
def decode_token(request):
	token = request.POST.get('token')
	try:
		payload = jwt_decode_handler(token)
	except jwt.ExpiredSignature:
		msg = _('Signature has expired.')
		raise serializers.ValidationError(msg)
	except jwt.DecodeError:
		msg = _('Error decoding signature.')
		raise serializers.ValidationError(msg)
	return JsonResponse({})

@api_view(['GET','POST'])
def update_profile(request):
	"""
	Used to update the profile informations of the user
	if user is authenticated.
	"""

	#parse the token payload to get user id
	payload = jwt_payload_handler(request.user)
	user_id = payload.get('user_id')
	if request.method == 'GET':
		#if request method 'GET' then return the user record
		#present in system
		response = {}
		response['status'] = status.HTTP_200_OK
		user_profile = model.get_user_details_for_update(user_id)
		response['data'] = user_profile
		return Response(response, status=response['status'])
	elif request.method == 'POST':
		#json response format
		response = {
			'status':None,
			'message':None,
			'errors':{},
			'data':{

			}
		}
		#validate the update profile form
		form_data = UpdateProfileForm(request.POST)
		if form_data.is_valid():
			form_data = form_data.cleaned_data
			form_data['user_name'] = '{0} {1}'.format(form_data['first_name'],form_data['last_name'])
			form_data['modified_date'] = datetime.now()
			#if form is valid update the new data to the user profile
			updated_details = model.update_user_details(user_id,form_data)
			if updated_details.modified_count:
				match = {'_id':ObjectId(user_id)}
				#dict used to define the fields that frontend need as projection
				projection ={
							'gender':1,
							'created_date':{
								'$dateToString':{
									'format': '%d-%m-%Y',#format the datetime obj to str
									'date':'$created_date'
								}
							},
							'_id':0,
							'user_name':1,'email':1,
							'first_name':1,'last_name':1,
							'role_name':'$role.display_name','portal_authenticate':1}
				#dict used to sort record based on key:value
				sort = {'created_date':1}
				data = model.get_users_records(match,projection,sort)
				response['message'] = 'Profile updated successfully.'
				response['data'] = data
				#if updated successfully then sent the info message.
				response['status'] = status.HTTP_200_OK
			else:
				response['status'] = status.HTTP_400_BAD_REQUEST
				response['message'] = 'Try again.'
		else:
			#if validation failed collect and return the errors.
			errors = dict(form_data.errors.items())
			response['status'] = status.HTTP_400_BAD_REQUEST
			response['message'] = 'Check form params.'
			response['errors'] = errors
		return Response(response, status=response['status'])

@api_view(['POST'])
def change_password(request):
	"""
	Used to change the logged in user password
	if they opt for change in password
	"""
	if request.method == 'POST':
		#json response format
		response = {
			'status':None,
			'message':None,
			'errors':{},
		}
		#parse the token payload to get the user id.
		payload = jwt_payload_handler(request.user)
		#user id from the payload
		user_email = payload.get('email')
		user_id = payload.get('user_id')
		form_data = ChangePasswordForm(request.POST)
		if form_data.is_valid():
			form_data = form_data.cleaned_data
			#get the hashed password of the user for the given email
			#if its exists in records.
			login_details = model.get_user_hashed_password(user_email)
			if login_details:
				given_password = form_data.get('current_password')
				new_password = form_data.get('new_password')
				#check the given password is matched with user hashed password or not.
				password_isvalid = check_password(given_password,login_details['password'])
				if password_isvalid:
					if login_details['is_active'] is False:
						response['message'] = 'User account is in inactive state.'
					update_data = {}
					#hashed the plain password using django password hasher
					update_data['password'] = make_password(new_password)
					update_data['modified_date'] = datetime.now()
					change_password_response = model.update_user_details(user_id,update_data)
					if change_password_response.modified_count:
						#if updated successfully then sent the info message.
						response['status'] = status.HTTP_200_OK
						response['message'] = 'Profile updated successfully.'
					else:
						response['status'] = status.HTTP_401_UNAUTHORIZED
						response['message'] = 'Try again.'
				else:
					response['status'] = status.HTTP_401_UNAUTHORIZED
					response['message'] = 'Incorrect password.'
					response['errors'] = 'Incorrect password.'
			else:
				response['status'] = status.HTTP_401_UNAUTHORIZED
				response['message'] = 'Login again to change password.'
				response['errors'] = {'form':'Login again to change password.'}
		else:
			response['message'] = 'Check form params.'
			response['status'] = status.HTTP_400_BAD_REQUEST
			response['errors'] = dict(form_data.errors.items())
		return Response(response, status=response['status'])

@api_view(['POST'])
@permission_classes((AllowAny,))
def forgot_password(request):
	"""
	Used to generate and sent password reset link to the
	user who opt for reset if their given email is valid.
	"""
	response = {
		'status':None,
		'message':None,
		'errors':{},
		'data':{}
	}
	if request.method == 'POST':
		form_data = ForgotPasswordForm(request.POST)
		if form_data.is_valid():
			form_data = form_data.cleaned_data
			#get the application running host 
			host = request.get_host()
			#get the given form email
			email  = form_data['email']
			#get user detail if email found in the system
			payload = model.get_user_payload_details(email)
			#if email found and user account is in active state
			#then allow for tokenization
			if payload and payload['is_active'] and payload['is_deleted'] is not True:
				#create the JWT token to sent along with the reset url
				#to verify the user
				payload = jwt_payload_handler(payload)
				#url to sent in mail for reset password
				reset_url = '{0}/verify_reset/{1}'.format(host,jwt_encode_handler(payload))
				#data to store in user record to verify the reset token in futures.
				data = {'password_reset':{
										'reset_link_sent':True,
										'opt_at':datetime.now(),
										'valid_till':datetime.fromtimestamp(payload['exp'])}
										}
				#update the above data in the user document
				user_opt_reset = model.mark_user_for_reset(email,data)
				#if update success sent the url to the user
				if user_opt_reset.modified_count:
					response['message'] = 'Open the one-time url to reset your password.'
					response['data']['reset_url'] = reset_url
					response['data']['instruction'] = 'Use this url to reset your password.'
					response['status'] = status.HTTP_200_OK
				else:#if not update successfully ask the user to try again
					response['message'] = 'Try again.'
					response['status'] = HTTP_400_BAD_REQUEST
			else:
				response['message'] = 'User account not found.'
				response['status'] = status.HTTP_404_NOT_FOUND
		else:
			response['message'] = 'Check the form fields.'
			response['errors'] = dict(form_data.errors.items())
			response['status'] = status.HTTP_400_BAD_REQUEST
		return Response(response, status=response['status'])

def validate_token(token):
	response = {
		'status':None,
		'message':None,
		'errors':{},
	}
	payload = {}
	#decode and verify the token from the reset link
	try:
		payload = jwt_decode_handler(token)
	except jwt.ExpiredSignature:
		response['message'] = 'Check your URL expiration time.'
		response['errors'] = 'Reset password link has expired.'
		response['status'] = status.HTTP_400_BAD_REQUEST
	except jwt.DecodeError:
		response['message'] = 'Check your URL.'
		response['errors'] = 'Invalid URL for reset password.'
		response['status'] = status.HTTP_400_BAD_REQUEST
	return (response,payload)

def check_token_validity(payload,response):
	#if token valid then find, that user_id found in payload 
	#is already opt for reset
	reset_enabled = model.is_user_opt_reset_password(payload['user_id'])
	url_exp = datetime.fromtimestamp(payload['exp'])
	#if reset opted already and if the token is latest one
	#then allow the user to change password to new.
	if reset_enabled and (reset_enabled['password_reset']['valid_till'] == url_exp):
		response['status'] = status.HTTP_200_OK
		response['message'] = 'Token verification success.'
	else:
		#if the token found older or invalid alert the errors
		response['status'] = status.HTTP_400_BAD_REQUEST
		response['message'] = 'Incorrect/Old URL found.'
		response['errors'] = 'Invalid URL for reset password.'
	return response
	
@api_view(['GET','POST'])
@permission_classes((AllowAny,))
def verfiy_forgot_password(request,token):
	"""
	Used to validate the reset password token to reset the user password
	if token valid and not expired.
	"""
	#check the token present in url is valid
	#if valid decode the token to find payload
	response,payload = map(dict,validate_token(token))
	if request.method == 'GET':
		#if payload found check that particular user
		#already requested for reset and chack the url is latest one
		if payload:
			#if valid sent the response to the form to accept new password
			response = check_token_validity(payload,response)
	elif request.method == 'POST':
		#if payload found check that particular user
		#already requested for reset and chack the url is latest one
		if payload:
			user_id = payload['user_id']
			response = check_token_validity(payload,response)
			if status.is_success(response['status']):
				form_data = ResetPasswordForm(request.data)
				#validate the form attributes
				if form_data.is_valid():
					#if all the form and token validations are success
					#then allow to change password
					#clean the input form data
					form_data = form_data.cleaned_data
					encrpt_password = make_password(form_data['new_password'])
					modified_date = datetime.now()
					#update and replace old password with newly genetated one
					data = {'password':encrpt_password,'modified_date':modified_date}
					#update the fields
					updated_details = model.update_user_details(user_id,data)
					#if once the password reset done then there is no scope
					#to maintain reset flag in database, remove the key:value
					remove_data = {'password_reset':1}
					unset_reset = model.unset_reset_tag(user_id,remove_data)
					#return the success info as a response
					if updated_details.modified_count:
						response['status'] = status.HTTP_200_OK
						response['message'] = 'Password reset successfully.'
					else:
						response['message'] = 'Try again.'
						response['status'] = status.HTTP_400_BAD_REQUEST
				else:
					#if validation failed collect the error and sent as a response
					response['status'] = status.HTTP_400_BAD_REQUEST
					response['message'] = 'Check your form fields.'
					response['errors'] = dict(form_data.errors.items())
		return Response(response, status=response['status'])

@api_view(['POST'])
def delete_user(request):
	if request.method == 'POST':
		response = {
			'status':None,
			'message':None,
			'errors':{}
		}
		#get the user_id from request to del
		user_id = request.data.get('user_id')
		if user_id:
			#if user_id present check the user opt for del or undo
			#if opt_for_delete is False then they opt for undo
			opt_for_delete = request.data.get('opt_for_delete',True)
			if isinstance(opt_for_delete,str):
				opt_for_delete = json.loads(opt_for_delete)
			data = {}
			#convert the str to bool and compare
			if opt_for_delete is True:
				data['is_deleted'] = True
			else:
				data['is_deleted'] = False
			data['modified_date'] = datetime.now()
			#update the delete status for the user
			delete_user_response = model.update_user_details(user_id,data)
			#if matching record found and record updated
			#then reply with success msg.
			if delete_user_response.modified_count:
				response['status'] = status.HTTP_200_OK
				if opt_for_delete is True:
					response['message'] = 'Account deleted successfully.'
				else:
					response['message'] = 'Account undo successfully.'
			else:
				response['status'] = status.HTTP_404_NOT_FOUND
				response['message'] = 'Account not found.'
				response['errors'] = 'Account not found.'
		else:
			request['status'] = status.HTTP_400_BAD_REQUEST
			response['message'] = 'Check request param.'
			response['errors'] = {'user_id':'user_id required.'}
		return Response(response, status=response['status'])

@api_view(['POST'])
def change_user_active_status(request):
	"""
	Used to toggle the current profile status of the user by user_id.
	"""
	if request.method == 'POST':
		response = {
			'success':False,
			'message':None,
			'errors':[]
		}
		#get the user_id to active/inactive account
		user_id = request.data.get('user_id',None)
		#get the current profile status of the user
		current_status = request.data.get('active',None)
		if (user_id is not None) and (current_status is not None):
			current_status = json.loads(current_status)
			data = {'is_active':True}
			#if the given state in active/inactive just toggle the status
			if current_status is True:
				data['is_active'] = False
			data['modified_date'] = datetime.now()
			#update the record into table
			update_user = model.update_user_details(user_id,data)
			if update_user.modified_count:
				#if record updated successfully return success response.
				response['status'] = status.HTTP_200_OK
				if current_status is True:
					response['message'] = 'Account made Inactive successfully.'
				else:
					response['message'] = 'Account made active successfully.'
			else:
				response['status'] = status.HTTP_404_NOT_FOUND
				#if record not found return the error status
				response['message'] = 'Account not found.'
				response['errors'] = 'Account not found.'
		else:
			response['status'] = status.HTTP_400_BAD_REQUEST
			#if mandatory args not found return the error codes
			response['message'] = 'Check request param.'
			if not user_id:
				response['errors'].append({'user_id':'Field is required.'})
			if not current_status:
				response['errors'].append({'active':'Field is required.'})
		return Response(response, status=response['status'])

@api_view(['GET','POST'])
def list_users(request):
	"""
	List the other users details of those who monitor by the logged in user
	"""
	response = {
		'status':status.HTTP_400_BAD_REQUEST,
		'data':[]
		}
	#parse the token payload to get the user id.
	payload = jwt_payload_handler(request.user)
	if request.method == 'GET':
		#user id from the payload
		logged_in_user_id = payload.get('user_id')
		limit = request.data.get('limit',10)
		page = request.data.get('page',1)
		#get logged-in user role detail
		logged_user_role = model.get_role_description(logged_in_user_id)
		#check the user role and permission level from settings
		if (logged_user_role and (logged_user_role['role']['role_name'] in APP_ACCESS_ROLES.keys())):
			user_control_roles = APP_ACCESS_ROLES[logged_user_role['role']['role_name']]
			#dict used to filter query result
			match = {'is_deleted':False,'role.role_name':{'$in':user_control_roles}}
			total_records = model.total_record_count(match)
			#dict used to define the fields that frontend need as projection
			projection ={'user_id':{
							'$convert':{
								'input':'$_id', #format the ObjectId to str
								'to':'string'
								}
							},
						'created_date':{
							'$dateToString':{
								'format': '%d-%m-%Y',#format the datetime obj to str
								'date':'$created_date'
								}
							},
						'_id':0,
						'user_name':1,'email':1,
						'role_name':'$role.display_name','portal_authenticate':1}
			#dict used to sort record based on key:value
			sort = {'created_date':1}
			#call the function to get the user records based on the params
			data = model.get_users_records(match,projection,sort,page,limit)
			response['status'] = status.HTTP_200_OK
			response['data'] = data
			response['count'] = len(data)
			response['total_records'] = total_records
	return Response(response, status=response['status'])

@api_view(['POST'])
def linkedin_account_login(request):
	if request.method == 'POST':
		response = {
			'status':None,
			'message':None,
			'errors':{}
		}
		#validate the LinkedIn login form
		form_data = LinkedInLoginForm(request.POST)
		if form_data.is_valid():
			form_data = form_data.cleaned_data
			script_response = linkedin_api.Login(form_data['email'],form_data['password'])
			if script_response['Login successful'] is True:
				response['status'] = status.HTTP_200_OK
				response['message'] = 'Connected with linkedin successfully.'
			else:
				response['errors'] = script_response['error']
				response['status'] = status.HTTP_401_UNAUTHORIZED
		else:
			response['status'] = status.HTTP_400_BAD_REQUEST
			response['errors'] = dict(form_data.errors.items())
		return Response(response, status=response['status'])

@api_view(['GET'])
def linkedin_search_candidate(request):
	if request.method == 'GET':
		response = {
			'status':None,
			'message':None,
			'errors':{},
			'data':[]
		}
		search_term  = request.data.get('search_term',None)
		search_term = search_term.strip()
		max_record = 10
		if search_term:
			response['status'] = status.HTTP_200_OK
			search_result = linkedin_api.search_candidate(search_term, max_record)
			print(search_result)
			if search_result['Search Candidates Successful'] is True:
				result = []
				for i in search_result['data']:
					i['summary'] = '-'
					result.append(i)
				response['data'] = result
				response['status'] = status.HTTP_200_OK
			else:
				response['message'] = 'Try again with a search keywords.'
				response['status'] = status.HTTP_400_BAD_REQUEST
				response['errors'] = {'error':'Try again.'}
		else:
			response['status'] = status.HTTP_400_BAD_REQUEST
			response['errors'] = {'search_term':'Required'}
		return Response(response, status=response['status'])

@api_view(['GET'])
def linkedin_view_profile(request):
	if request.method == 'GET':
		response = {
			'status':None,
			'message':None,
			'errors':{},
			'data':[]
		}
		candidate_profile_url = request.data.get('profile_url','')
		candidate_profile_url = candidate_profile_url.strip()
		if candidate_profile_url:
			candidate_profile_detail = linkedin_api.scrap_candidate(candidate_profile_url)
			if candidate_profile_detail:
				print(candidate_profile_detail)
			else:
				response['status'] = status.HTTP_404_NOT_FOUND
				response['message'] = 'Enter valid profile URL.'
				response['errors'] = {'profile_url':'No matching user for the given URL.'}
				print(candidate_profile_detail)
		else:
			response['status'] = status.HTTP_400_BAD_REQUEST
			response['message'] = 'profile_url is required.'
			response['errors'] = {'profile_url':'Valid URL is required.'}
		return Response(response, status=response['status'])

@api_view(['POST'])
def add_profile_status(request):
	response = {
		'status':None,
		'message':None,
		'errors':{},
	}
	if request.method == 'POST':
		form_data = request.POST
		insert_data = {}
		status_name = form_data.get('status_name',None)
		if status_name:
			status_name = ((status_name.strip()).lower()).title()
			insert_data = {
						'status_name':status_name,
						'is_active':True,
						'is_deleted':False,
						'created_date':datetime.now(),
						'modified_date':datetime.now(),
					}
			result = model.insert_new_profile_status(insert_data)
			if result.inserted_id:
				response['status'] = status.HTTP_200_OK
				response['message'] = 'Record inserted successfully.'
			else:
				response['status'] = status.HTTP_404_NOT_FOUND
				response['message'] = 'Try again.'
				response['error'] = {'status_name':'Try again'}
		else:
			response['status'] = status.HTTP_400_BAD_REQUEST
			response['message'] = 'Enter valid status name.'
			response['errors'] = {'status_name':'Enter valid status name.'}
	return Response(response,status=response['status'])

@api_view(['POST'])
def add_profile_label(request):
	response = {
		'status':None,
		'message':None,
		'errors':{},
	}
	if request.method == 'POST':
		form_data = request.POST
		label_name = form_data.get('label_name',None)
		if label_name:
			label_name = ((label_name.strip()).lower()).title()
			insert_data = {
						'label_name':label_name,
						'is_active':True,
						'is_deleted':False,
						'created_date':datetime.now(),
						'modified_date':datetime.now(),
					}
			result = model.insert_new_profile_label(insert_data)
			if result.inserted_id:
				response['status'] = status.HTTP_200_OK
				response['message'] = 'Record inserted successfully.'
			else:
				response['status'] = status.HTTP_404_NOT_FOUND
				response['message'] = 'Try again.'
				response['error'] = {'label_name':'Try again'}
		else:
			response['status'] = status.HTTP_400_BAD_REQUEST
			response['message'] = 'Enter valid label name.'
			response['errors'] = {'label_name':'Enter valid label name.'}
	return Response(response,status=response['status'])