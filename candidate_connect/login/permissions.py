from rest_framework.permissions import BasePermission
class IsAuthenticated(BasePermission):
    """
    Allows access only to authenticated users who gave
    JWT Authorization header in request.
    """

    def has_permission(self, request, view):
        authorization_header = request.META.get('HTTP_AUTHORIZATION',None)
        if authorization_header is not None:
            return True
        else:
            return False
        #return bool(request.user and request.user.is_authenticated)