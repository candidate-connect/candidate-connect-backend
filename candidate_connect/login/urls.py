from django.urls import path
from rest_framework_jwt.views import ObtainJSONWebToken

from . import views
# from .serializer import CustomJWTSerializer

urlpatterns = [
    path('add_profile_status/', views.add_profile_status, name = "add_profile_status"),
    path('add_profile_label/', views.add_profile_label, name = "add_profile_label"),
    path('check_email_exist/', views.is_signup_email_exists, name = "is_signup_email_exists"),
    path('change_password/', views.change_password, name = "change_password"),
    path('change_user_active_status/', views.change_user_active_status, name = "change_user_active_status"),
    path('decode_token/', views.decode_token, name = "decode_token"),
    path('delete_user/', views.delete_user, name = "delete_user"),
    path('forgot_password/', views.forgot_password, name = "forgot_password"),
    path('login/', views.login, name = "login"),
    path('list_users/', views.list_users, name="list_users"),
    path('linkedin_login/', views.linkedin_account_login, name="linkedin_account_login"),
    path('linkedin_search_profile/', views.linkedin_search_candidate, name="linkedin_search_candidate"),
    path('linkedin_view_profile/', views.linkedin_view_profile, name="linkedin_view_profile"),
    # path('login/', ObtainJSONWebToken.as_view(serializer_class=CustomJWTSerializer), name = "logi"),
    path('make_password/', views.generate_password, name = "make_password"),
    path('posts/', views.posts, name = "posts"),
    path('signup/', views.signup, name = "signup"),
    path('update_profile/', views.update_profile, name = "update_profile"),
    path('verify_reset/<token>', views.verfiy_forgot_password, name = "verfiy_forgot_password"),
    path('validate/', views.validate_password, name = "validate"),

]
#jenkin-password
#cbf4c28bce344c8f9b9be1f00e04f069