from django import forms
from .models import Model
model = Model()

class LoginForm(forms.Form):


	email = forms.EmailField(
		required = True, 
		min_length = 6,
		max_length = 50,
		error_messages = {
							'required': 'Email address required.',
							'max_length': 'Email should not be more than 50 characters.'
							}
		)
	password = forms.CharField(
		required = True,
		min_length = 6,
		max_length = 20,
		widget = forms.PasswordInput(),
		error_messages = {
							'required': 'Password required.',
							'min_length': 'The password must be alteast 6 characters.',
							'max_length': 'Maximum length should not be more than 20 characters.'
							}
		)

class SignUpForm(forms.Form):

	first_name = forms.CharField(
		required = True,
		max_length = 50,
		error_messages = {
							'required':'First Name required.',
							'max_length':'Maximum length should not be more than 50 characters.'
							} 
		)
	last_name = forms.CharField(
		required = True,
		max_length = 50,
		error_messages = {
							'required':'Last Name required.',
							'max_length':'Maximum length should not be more than 50 characters.'
							}
		)
	email = forms.EmailField(
		max_length = 50,
		required = True,
		error_messages = {
							'required':'Email address required',
							'max_length':'Email should not be more than 50 characters.',
							}
		)
	password = forms.CharField(
		required = True,
		min_length = 6,
		max_length = 20,
		widget = forms.PasswordInput(),
		error_messages = {
							'required':'Password required.',
							'min_length': 'The password must be alteast 6 characters.',
							'max_length': 'Maximum length should not be more than 20 characters.'
							}
		)
	confirm_password = forms.CharField(
		required = True,
		min_length = 6,
		max_length = 20,
		widget = forms.PasswordInput(),
		error_messages = {
							'required':'Confirm Password required.',
							'min_length':'The password must be alteast 6 characters.',
							'max_length':'Maximum length should not be more than 20 characters.'
							}
		)

	def get_roles():
		"""Used to get the different types of roles in users
		to create a roles selection"""
		roles = model.get_users_role()
		roles_tuple = [(role['role_name'],role['display_name']) for role in roles]
		return (roles_tuple)

	role = forms.ChoiceField(
		required = True,
		choices = get_roles,
		)

	OPTION = (('M','Male'),('F','Female'))
	gender = forms.ChoiceField(
		required = True,
		choices = OPTION,) 
	# this function will be used for the validation 
	def clean(self): 
		# data from the form is fetched using super function 
		super(SignUpForm, self).clean()
		# extract form data 
		password = self.cleaned_data.get('password','') 
		confirm_password = self.cleaned_data.get('confirm_password','')
		email = self.cleaned_data.get('email',None)

		#for confirm password
		if(password != confirm_password):
			self._errors['confirm_password'] = self.error_class(['Passwords does not match.'])

		#to check duplicate mail id
		count = model.is_email_present(email)
		if count:
			self._errors['email'] = self.error_class(['Email already exists in records'])
		# return any errors if found 
		return self.cleaned_data

class UpdateProfileForm(forms.Form):

	first_name = forms.CharField(
		required = True,
		max_length = 50,
		error_messages = {
							'required':'First Name required.',
							'max_length':'Maximum length should not be more than 50 characters.'
							} 
		)
	last_name = forms.CharField(
		required = True,
		max_length = 50,
		error_messages = {
							'required':'Last Name required.',
							'max_length':'Maximum length should not be more than 50 characters.'
							}
		)
	
	OPTION = (('M','Male'),('F','Female'))
	gender = forms.ChoiceField(
		required = True,
		choices = OPTION,)

class ChangePasswordForm(forms.Form):
	current_password = forms.CharField(
					required = True,
					min_length = 6,
					max_length = 20,
					widget = forms.PasswordInput(),
					error_messages = {
						'required':'Password required.',
						'min_length': 'The password must be alteast 6 characters.',
						'max_length': 'Maximum length should not be more than 20 characters.'
						}
					)

	new_password = forms.CharField(
					required = True,
					min_length = 6,
					max_length = 20,
					widget = forms.PasswordInput(),
					error_messages = {
						'required':'New password required.',
						'min_length': 'The new password must be alteast 6 characters.',
						'max_length': 'Maximum length should not be more than 20 characters.'
						}
					)

	confirm_password = forms.CharField(
					required = True,
					min_length = 6,
					max_length = 20,
					widget = forms.PasswordInput(),
					error_messages = {
						'required':'Confirm password required.',
						'min_length':'The confirm password must be alteast 6 characters.',
						'max_length':'Maximum length should not be more than 20 characters.'
						}
					)

	# this function will be used for the validation 
	def clean(self): 
		# extract form data 
		current_password = self.cleaned_data.get('current',None) 
		new_password = self.cleaned_data.get('new_password',None)
		confirm_password = self.cleaned_data.get('confirm_password',None)
		if confirm_password is not None:
		#for confirm password
			if(new_password != confirm_password):
				self._errors['confirm_password'] = self.error_class(['Passwords does not match.'])
		# return any errors if found 
		return self.cleaned_data

class ForgotPasswordForm(forms.Form):
	email = forms.EmailField(
					max_length = 50,
					required = True,
					error_messages = {
							'required':'Email address required',
							'max_length':'Email should not be more than 50 characters.',
							}
						)

	def clean_email(self):
		email = self.cleaned_data.get('email',None)
		#to check duplicate mail id
		count = model.is_email_present(email)
		if count:
			return email
		else:
			self._errors['email'] = self.error_class(['Email not found in the records.'])
			return self.cleaned_data

class ResetPasswordForm(forms.Form):

	new_password = forms.CharField(
		required = True,
		min_length = 6,
		max_length = 20,
		widget = forms.PasswordInput(),
		error_messages = {
							'required':'Password required.',
							'min_length': 'The password must be alteast 6 characters.',
							'max_length': 'Maximum length should not be more than 20 characters.'
							}
		)
	confirm_password = forms.CharField(
		required = True,
		min_length = 6,
		max_length = 20,
		widget = forms.PasswordInput(),
		error_messages = {
							'required':'Confirm Password required.',
							'min_length':'The password must be alteast 6 characters.',
							'max_length':'Maximum length should not be more than 20 characters.'
							}
		)

	def clean(self):
		# extract form data 
		new_password = self.cleaned_data.get('new_password')
		confirm_password = self.cleaned_data.get('confirm_password')

		#for confirm password
		if(new_password != confirm_password):
			self._errors['confirm_password'] = self.error_class(['Passwords does not match.'])
		# return any errors if found 
		return self.cleaned_data

class LinkedInLoginForm(forms.Form):
	email = forms.EmailField(
		required = True, 
		error_messages = {
							'required': 'Email/Phone number required.',
						}
		)
	password = forms.CharField(
		required = True,
		widget = forms.PasswordInput(),
		error_messages = {
							'required': 'Password required.',
						}
		)		