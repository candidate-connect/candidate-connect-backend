import pymongo
from pymongo import MongoClient
from bson.objectid import ObjectId

from db_config.connection import db

class Model():
	def __init__(self):
		self.db = db
	def insert_signup_data(self,data):
		inserted_row = self.db.candidate_connect_users.insert_one(data)
		return True

	def get_user_role_details(self,role_name):
		user_role = self.db.candidate_connect_roles.find_one({'role_name':role_name})
		return user_role

	def is_email_present(self,email):
		email_count = self.db.candidate_connect_users.count_documents({'email':email,'is_deleted':False})
		return email_count

	def get_user_hashed_password(self,email):
		user_detail = self.db.candidate_connect_users.find_one(
			{'email':email,'is_deleted':False},
			{'_id':1,'email':1,'password':1,'is_active':1,'self_authenticate':1})
		return user_detail

	def get_user_payload_details(self,user_email):
		payload_data = self.db.candidate_connect_users.find_one(
			{'email':user_email},
			{'_id':1,'user_name':1,'email':1,'is_active':1,'is_deleted':1})
		return payload_data

	def get_users_role(self):
		users_role = self.db.candidate_connect_roles.find(
			{'role_name':{'$ne':'admin'}},
			{'_id':0})
		return users_role

	def get_user_details_for_update(self,user_id):
		user_details = self.db.candidate_connect_users.find_one(
			{'_id':ObjectId(user_id)},
			{'_id':0,'first_name':1,'last_name':1,'user_name':1,'gender':1})
		return user_details

	def update_user_details(self,user_id,user_data):
		user_details = self.db.candidate_connect_users.update_one(
			{'_id':ObjectId(user_id)},
			{'$set':user_data})
		return user_details

	def mark_user_for_reset(self,user_email,data):
		user_opt_reset_pwd = self.db.candidate_connect_users.update_one(
			{'email':user_email},
			{'$set':data})
		return user_opt_reset_pwd

	def is_user_opt_reset_password(self,user_id):
		reset_pwd_info = self.db.candidate_connect_users.find_one(
			{'_id':ObjectId(user_id),'password_reset':{ '$exists':True,'$ne':None}},
			{'password_reset':1})
		return reset_pwd_info

	def unset_reset_tag(self,user_id,data):
		unset_reset_pwd = self.db.candidate_connect_users.update_one(
			{'_id':ObjectId(user_id)},
			{'$unset':data})
		return unset_reset_pwd

	def get_role_description(self,logged_in_user_id):
		role_decription = self.db.candidate_connect_users.find_one(
			{'_id':ObjectId(logged_in_user_id)},
			{'_id':0,'role':1})
		return role_decription

	def get_users_records(self,match,project,sort,page=1,limit=0):
		page,limit = int(page),int(limit)
		query_terms = [{'$match':match},{'$project':project},{'$sort':sort}]
		if limit != 0:
			query_terms.extend([{'$skip':(page*limit)-limit},{'$limit':limit}])
		result = list(db.candidate_connect_users.aggregate(query_terms))
		return result

	def total_record_count(self,data):
		email_count = self.db.candidate_connect_users.count_documents(data)
		return email_count

	def insert_new_profile_status(self,data):
		inserted_row = self.db.candidate_connect_profile_status.insert_one(data)
		return inserted_row

	def insert_new_profile_label(self,data):
		inserted_row = self.db.candidate_connect_profile_label.insert_one(data)
		return inserted_row