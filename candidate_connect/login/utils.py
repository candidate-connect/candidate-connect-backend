from calendar import timegm
from datetime import datetime

from rest_framework_jwt.settings import api_settings

def jwt_payload_handler(user):
    """ Used to generate custom JWT payload for the token
    """
    payload = {
        'user_id':str(user['_id']),
        'username':user['user_name'],
        'exp':datetime.utcnow() + api_settings.JWT_EXPIRATION_DELTA
    }
    if user.get('email'):
        payload['email'] = user['email']

    # Include original issued at time for a brand new token,
    # to allow token refresh
    if api_settings.JWT_ALLOW_REFRESH:
        payload['orig_iat'] = timegm(
            datetime.utcnow().utctimetuple()
        )

    if api_settings.JWT_AUDIENCE is not None:
        payload['aud'] = api_settings.JWT_AUDIENCE

    if api_settings.JWT_ISSUER is not None:
        payload['iss'] = api_settings.JWT_ISSUER

    return payload