from django.utils.translation import ugettext as _
from rest_framework import exceptions
from rest_framework_jwt.authentication import JSONWebTokenAuthentication

from .models import Model
model = Model()

class CustomJSONWebTokenAuthentication(JSONWebTokenAuthentication):
    def authenticate_credentials(self, payload):

        if 'email' not in payload.keys():
            msg = _('Invalid payload.')
            raise exceptions.AuthenticationFailed(msg)

        user = model.get_user_payload_details(payload['email'])
        if not user:
            msg = _('Invalid signature.')
            raise exceptions.AuthenticationFailed(msg)

        if user['is_active'] is not True:
            msg = _('User account is disabled.')
            raise exceptions.AuthenticationFailed(msg)

        if user['is_deleted'] is True:
            msg = _('Active user account has not found.')
            raise exceptions.AuthenticationFailed(msg)
        return user